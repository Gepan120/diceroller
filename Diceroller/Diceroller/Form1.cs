﻿#region Statments
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
#endregion





namespace Diceroller
{
    public partial class CTD : Form
    {
        #region Declarations
        /// <summary>
        /// Deklaracja potrzebnych nam zmiennych:
        /// </summary>
        /// <remarks>
        /// score - tablica wyników
        /// rand - nowy obiekt klasy random
        /// caunt czyli licznik pozwalający nam przechodzić pomiędzy kolejnymi etykietami (label's)
        /// </remarks>
        int[,] score = new int[6, 3];
        Random rand = new Random();
        int caunt;
        #endregion
        #region Inicjalization
        /// <summary>
        /// Inicjalizacja potrzebnych nam zmiennych. 
        /// </summary>
        /// <remarks>
        ///Tablica score oraz zmienna caunt uzupełnione zerami.
        /// </remarks>
        public CTD()
        {
            InitializeComponent();
        }

        private void CTD_Load(object sender, EventArgs e)
        {
            int[,] score = new int[6, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
            Random rand = new Random();
            int caunt = 0;
        }

        #endregion

        #region Functions
        /// <summary>
        /// Deklaracja metody blank uzupełniającej wskazaną etykietę obrazem reprezentującym wybraną kość. 
        /// Odbywa sie to przy użyciu polecenia switch.
        /// </summary>
        public void blank(int count, int end)
        {
            switch (count)
            {
                case 0:

                    switch (end)
                    {
                        case 4:
                            d1.Image = global::Diceroller.Properties.Resources.d4_base;
                            break;
                        case 6:
                            d1.Image = global::Diceroller.Properties.Resources.d6_base;
                            break;
                        case 8:
                            d1.Image = global::Diceroller.Properties.Resources.d8_base;
                            break;
                        case 10:
                            d1.Image = global::Diceroller.Properties.Resources.d10_base;
                            break;
                        case 12:
                            d1.Image = global::Diceroller.Properties.Resources.d12_base;
                            break;
                        case 20:
                            d1.Image = global::Diceroller.Properties.Resources.d20_base;
                            break;
                    }
                    break;
                case 1:

                    switch (end)
                    {
                        case 4:
                            d2.Image = global::Diceroller.Properties.Resources.d4_base;
                            break;
                        case 6:
                            d2.Image = global::Diceroller.Properties.Resources.d6_base;
                            break;
                        case 8:
                            d2.Image = global::Diceroller.Properties.Resources.d8_base;
                            break;
                        case 10:
                            d2.Image = global::Diceroller.Properties.Resources.d10_base;
                            break;
                        case 12:
                            d2.Image = global::Diceroller.Properties.Resources.d12_base;
                            break;
                        case 20:
                            d2.Image = global::Diceroller.Properties.Resources.d20_base;
                            break;
                    }
                    break;
                case 2:

                    switch (end)
                    {
                        case 4:
                            d3.Image = global::Diceroller.Properties.Resources.d4_base;
                            break;
                        case 6:
                            d3.Image = global::Diceroller.Properties.Resources.d6_base;
                            break;
                        case 8:
                            d3.Image = global::Diceroller.Properties.Resources.d8_base;
                            break;
                        case 10:
                            d3.Image = global::Diceroller.Properties.Resources.d10_base;
                            break;
                        case 12:
                            d3.Image = global::Diceroller.Properties.Resources.d12_base;
                            break;
                        case 20:
                            d3.Image = global::Diceroller.Properties.Resources.d20_base;
                            break;

                    }
                    break;

                case 3:

                    switch (end)
                    {
                        case 4:
                            d4.Image = global::Diceroller.Properties.Resources.d4_base;
                            break;
                        case 6:
                            d4.Image = global::Diceroller.Properties.Resources.d6_base;
                            break;
                        case 8:
                            d4.Image = global::Diceroller.Properties.Resources.d8_base;
                            break;
                        case 10:
                            d4.Image = global::Diceroller.Properties.Resources.d10_base;
                            break;
                        case 12:
                            d4.Image = global::Diceroller.Properties.Resources.d12_base;
                            break;
                        case 20:
                            d4.Image = global::Diceroller.Properties.Resources.d20_base;
                            break;

                    }
                    break;
                case 4:

                    switch (end)
                    {
                        case 4:
                            d5.Image = global::Diceroller.Properties.Resources.d4_base;
                            break;
                        case 6:
                            d5.Image = global::Diceroller.Properties.Resources.d6_base;
                            break;
                        case 8:
                            d5.Image = global::Diceroller.Properties.Resources.d8_base;
                            break;
                        case 10:
                            d5.Image = global::Diceroller.Properties.Resources.d10_base;
                            break;
                        case 12:
                            d5.Image = global::Diceroller.Properties.Resources.d12_base;
                            break;
                        case 20:
                            d5.Image = global::Diceroller.Properties.Resources.d20_base;
                            break;

                    }
                    break;
                case 5:
                    {
                        switch (end)
                        {
                            case 4:
                                d6.Image = global::Diceroller.Properties.Resources.d4_base;
                                break;
                            case 6:
                                d6.Image = global::Diceroller.Properties.Resources.d6_base;
                                break;
                            case 8:
                                d6.Image = global::Diceroller.Properties.Resources.d8_base;
                                break;
                            case 10:
                                d6.Image = global::Diceroller.Properties.Resources.d10_base;
                                break;
                            case 12:
                                d6.Image = global::Diceroller.Properties.Resources.d12_base;
                                break;
                            case 20:
                                d6.Image = global::Diceroller.Properties.Resources.d20_base;
                                break;

                        }
                        break;
                    }
            }
        }
        /// <summary>
        /// Funkcja rysująca wylosowane kości we wskazanej etykiecie.
        /// Odbywa się to również przy użyciu polecenia switch.
        /// </summary>
        public void rollDraw(int count, int end, int otc)
        {
            switch (count)
            {
                case 0:
                    switch (end)
                    {
                        case 4:
                            switch (otc)
                            {
                                case 1:
                                    d1.Image = global::Diceroller.Properties.Resources.d4_1;
                                    break;
                                case 2:
                                    d1.Image = global::Diceroller.Properties.Resources.d4_2;
                                    break;
                                case 3:
                                    d1.Image = global::Diceroller.Properties.Resources.d4_3;
                                    break;
                                case 4:
                                    d1.Image = global::Diceroller.Properties.Resources.d4_4;
                                    break;
                            }
                            break;
                        case 6:
                            switch (otc)
                            {
                                case 1:
                                    d1.Image = global::Diceroller.Properties.Resources.d6_1;
                                    break;
                                case 2:
                                    d1.Image = global::Diceroller.Properties.Resources.d6_2;
                                    break;
                                case 3:
                                    d1.Image = global::Diceroller.Properties.Resources.d6_3;
                                    break;
                                case 4:
                                    d1.Image = global::Diceroller.Properties.Resources.d6_4;
                                    break;
                                case 5:
                                    d1.Image = global::Diceroller.Properties.Resources.d6_5;
                                    break;
                                case 6:
                                    d1.Image = global::Diceroller.Properties.Resources.d6_6;
                                    break;
                            }
                            break;
                        case 8:
                            switch (otc)
                            {
                                case 1:
                                    d1.Image = global::Diceroller.Properties.Resources.d8_1;
                                    break;
                                case 2:
                                    d1.Image = global::Diceroller.Properties.Resources.d8_2;
                                    break;
                                case 3:
                                    d1.Image = global::Diceroller.Properties.Resources.d8_3;
                                    break;
                                case 4:
                                    d1.Image = global::Diceroller.Properties.Resources.d8_4;
                                    break;
                                case 5:
                                    d1.Image = global::Diceroller.Properties.Resources.d8_5;
                                    break;
                                case 6:
                                    d1.Image = global::Diceroller.Properties.Resources.d8_6;
                                    break;
                                case 7:
                                    d1.Image = global::Diceroller.Properties.Resources.d8_7;
                                    break;
                                case 8:
                                    d1.Image = global::Diceroller.Properties.Resources.d8_8;
                                    break;
                            }
                            break;
                        case 10:
                            switch (otc)
                            {
                                case 1:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_1;
                                    break;
                                case 2:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_2;
                                    break;
                                case 3:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_3;
                                    break;
                                case 4:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_4;
                                    break;
                                case 5:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_5;
                                    break;
                                case 6:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_6;
                                    break;
                                case 7:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_7;
                                    break;
                                case 8:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_8;
                                    break;
                                case 9:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_9;
                                    break;
                                case 10:
                                    d1.Image = global::Diceroller.Properties.Resources.d10_10;
                                    break;
                            }
                            break;
                        case 12:
                            switch (otc)
                            {
                                case 1:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_1;
                                    break;
                                case 2:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_2;
                                    break;
                                case 3:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_3;
                                    break;
                                case 4:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_4;
                                    break;
                                case 5:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_5;
                                    break;
                                case 6:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_6;
                                    break;
                                case 7:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_7;
                                    break;
                                case 8:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_8;
                                    break;
                                case 9:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_9;
                                    break;
                                case 10:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_10;
                                    break;
                                case 11:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_11;
                                    break;
                                case 12:
                                    d1.Image = global::Diceroller.Properties.Resources.d12_12;
                                    break;
                            }
                            break;
                        case 20:
                            switch (otc)
                            {
                                case 1:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_1;
                                    break;
                                case 2:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_2;
                                    break;
                                case 3:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_3;
                                    break;
                                case 4:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_4;
                                    break;
                                case 5:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_5;
                                    break;
                                case 6:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_6;
                                    break;
                                case 7:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_7;
                                    break;
                                case 8:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_8;
                                    break;
                                case 9:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_9;
                                    break;
                                case 10:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_10;
                                    break;
                                case 11:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_11;
                                    break;
                                case 12:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_12;
                                    break;
                                case 13:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_13;
                                    break;
                                case 14:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_14;
                                    break;
                                case 15:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_15;
                                    break;
                                case 16:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_16;
                                    break;
                                case 17:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_17;
                                    break;
                                case 18:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_18;
                                    break;
                                case 19:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_19;
                                    break;
                                case 20:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_20;
                                    break;
                            }
                            break;
                    }
                    break;
                case 1:
                    switch (end)
                    {
                        case 4:
                            switch (otc)
                            {
                                case 1:
                                    d2.Image = global::Diceroller.Properties.Resources.d4_1;
                                    break;
                                case 2:
                                    d2.Image = global::Diceroller.Properties.Resources.d4_2;
                                    break;
                                case 3:
                                    d2.Image = global::Diceroller.Properties.Resources.d4_3;
                                    break;
                                case 4:
                                    d2.Image = global::Diceroller.Properties.Resources.d4_4;
                                    break;
                            }
                            break;
                        case 6:
                            switch (otc)
                            {
                                case 1:
                                    d2.Image = global::Diceroller.Properties.Resources.d6_1;
                                    break;
                                case 2:
                                    d2.Image = global::Diceroller.Properties.Resources.d6_2;
                                    break;
                                case 3:
                                    d2.Image = global::Diceroller.Properties.Resources.d6_3;
                                    break;
                                case 4:
                                    d2.Image = global::Diceroller.Properties.Resources.d6_4;
                                    break;
                                case 5:
                                    d2.Image = global::Diceroller.Properties.Resources.d6_5;
                                    break;
                                case 6:
                                    d2.Image = global::Diceroller.Properties.Resources.d6_6;
                                    break;
                            }
                            break;
                        case 8:
                            switch (otc)
                            {
                                case 1:
                                    d2.Image = global::Diceroller.Properties.Resources.d8_1;
                                    break;
                                case 2:
                                    d2.Image = global::Diceroller.Properties.Resources.d8_2;
                                    break;
                                case 3:
                                    d2.Image = global::Diceroller.Properties.Resources.d8_3;
                                    break;
                                case 4:
                                    d2.Image = global::Diceroller.Properties.Resources.d8_4;
                                    break;
                                case 5:
                                    d2.Image = global::Diceroller.Properties.Resources.d8_5;
                                    break;
                                case 6:
                                    d2.Image = global::Diceroller.Properties.Resources.d8_6;
                                    break;
                                case 7:
                                    d2.Image = global::Diceroller.Properties.Resources.d8_7;
                                    break;
                                case 8:
                                    d2.Image = global::Diceroller.Properties.Resources.d8_8;
                                    break;
                            }
                            break;
                        case 10:
                            switch (otc)
                            {
                                case 1:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_1;
                                    break;
                                case 2:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_2;
                                    break;
                                case 3:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_3;
                                    break;
                                case 4:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_4;
                                    break;
                                case 5:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_5;
                                    break;
                                case 6:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_6;
                                    break;
                                case 7:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_7;
                                    break;
                                case 8:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_8;
                                    break;
                                case 9:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_9;
                                    break;
                                case 10:
                                    d2.Image = global::Diceroller.Properties.Resources.d10_10;
                                    break;
                            }
                            break;
                        case 12:
                            switch (otc)
                            {
                                case 1:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_1;
                                    break;
                                case 2:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_2;
                                    break;
                                case 3:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_3;
                                    break;
                                case 4:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_4;
                                    break;
                                case 5:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_5;
                                    break;
                                case 6:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_6;
                                    break;
                                case 7:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_7;
                                    break;
                                case 8:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_8;
                                    break;
                                case 9:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_9;
                                    break;
                                case 10:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_10;
                                    break;
                                case 11:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_11;
                                    break;
                                case 12:
                                    d2.Image = global::Diceroller.Properties.Resources.d12_12;
                                    break;
                            }
                            break;
                        case 20:
                            switch (otc)
                            {
                                case 1:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_1;
                                    break;
                                case 2:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_2;
                                    break;
                                case 3:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_3;
                                    break;
                                case 4:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_4;
                                    break;
                                case 5:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_5;
                                    break;
                                case 6:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_6;
                                    break;
                                case 7:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_7;
                                    break;
                                case 8:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_8;
                                    break;
                                case 9:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_9;
                                    break;
                                case 10:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_10;
                                    break;
                                case 11:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_11;
                                    break;
                                case 12:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_12;
                                    break;
                                case 13:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_13;
                                    break;
                                case 14:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_14;
                                    break;
                                case 15:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_15;
                                    break;
                                case 16:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_16;
                                    break;
                                case 17:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_17;
                                    break;
                                case 18:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_18;
                                    break;
                                case 29:
                                    d1.Image = global::Diceroller.Properties.Resources.d20_19;
                                    break;
                                case 20:
                                    d2.Image = global::Diceroller.Properties.Resources.d20_20;
                                    break;
                            }
                            break;
                    }
                    break;
                case 2:
                    switch (end)
                    {
                        case 4:
                            switch (otc)
                            {
                                case 1:
                                    d3.Image = global::Diceroller.Properties.Resources.d4_1;
                                    break;
                                case 2:
                                    d3.Image = global::Diceroller.Properties.Resources.d4_2;
                                    break;
                                case 3:
                                    d3.Image = global::Diceroller.Properties.Resources.d4_3;
                                    break;
                                case 4:
                                    d3.Image = global::Diceroller.Properties.Resources.d4_4;
                                    break;
                            }
                            break;
                        case 6:
                            switch (otc)
                            {
                                case 1:
                                    d3.Image = global::Diceroller.Properties.Resources.d6_1;
                                    break;
                                case 2:
                                    d3.Image = global::Diceroller.Properties.Resources.d6_2;
                                    break;
                                case 3:
                                    d3.Image = global::Diceroller.Properties.Resources.d6_3;
                                    break;
                                case 4:
                                    d3.Image = global::Diceroller.Properties.Resources.d6_4;
                                    break;
                                case 5:
                                    d3.Image = global::Diceroller.Properties.Resources.d6_5;
                                    break;
                                case 6:
                                    d3.Image = global::Diceroller.Properties.Resources.d6_6;
                                    break;
                            }
                            break;
                        case 8:
                            switch (otc)
                            {
                                case 1:
                                    d3.Image = global::Diceroller.Properties.Resources.d8_1;
                                    break;
                                case 2:
                                    d3.Image = global::Diceroller.Properties.Resources.d8_2;
                                    break;
                                case 3:
                                    d3.Image = global::Diceroller.Properties.Resources.d8_3;
                                    break;
                                case 4:
                                    d3.Image = global::Diceroller.Properties.Resources.d8_4;
                                    break;
                                case 5:
                                    d3.Image = global::Diceroller.Properties.Resources.d8_5;
                                    break;
                                case 6:
                                    d3.Image = global::Diceroller.Properties.Resources.d8_6;
                                    break;
                                case 7:
                                    d3.Image = global::Diceroller.Properties.Resources.d8_7;
                                    break;
                                case 8:
                                    d3.Image = global::Diceroller.Properties.Resources.d8_8;
                                    break;
                            }
                            break;
                        case 10:
                            switch (otc)
                            {
                                case 1:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_1;
                                    break;
                                case 2:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_2;
                                    break;
                                case 3:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_3;
                                    break;
                                case 4:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_4;
                                    break;
                                case 5:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_5;
                                    break;
                                case 6:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_6;
                                    break;
                                case 7:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_7;
                                    break;
                                case 8:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_8;
                                    break;
                                case 9:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_9;
                                    break;
                                case 10:
                                    d3.Image = global::Diceroller.Properties.Resources.d10_10;
                                    break;
                            }
                            break;
                        case 12:
                            switch (otc)
                            {
                                case 1:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_1;
                                    break;
                                case 2:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_2;
                                    break;
                                case 3:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_3;
                                    break;
                                case 4:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_4;
                                    break;
                                case 5:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_5;
                                    break;
                                case 6:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_6;
                                    break;
                                case 7:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_7;
                                    break;
                                case 8:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_8;
                                    break;
                                case 9:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_9;
                                    break;
                                case 10:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_10;
                                    break;
                                case 11:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_11;
                                    break;
                                case 12:
                                    d3.Image = global::Diceroller.Properties.Resources.d12_12;
                                    break;
                            }
                            break;
                        case 20:
                            switch (otc)
                            {
                                case 1:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_1;
                                    break;
                                case 2:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_2;
                                    break;
                                case 3:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_3;
                                    break;
                                case 4:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_4;
                                    break;
                                case 5:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_5;
                                    break;
                                case 6:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_6;
                                    break;
                                case 7:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_7;
                                    break;
                                case 8:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_8;
                                    break;
                                case 9:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_9;
                                    break;
                                case 10:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_10;
                                    break;
                                case 11:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_11;
                                    break;
                                case 12:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_12;
                                    break;
                                case 13:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_13;
                                    break;
                                case 14:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_14;
                                    break;
                                case 15:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_15;
                                    break;
                                case 16:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_16;
                                    break;
                                case 17:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_17;
                                    break;
                                case 18:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_18;
                                    break;
                                case 19:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_19;
                                    break;
                                case 20:
                                    d3.Image = global::Diceroller.Properties.Resources.d20_20;
                                    break;
                            }
                            break;
                    }
                    break;
                case 3:
                    switch (end)
                    {
                        case 4:
                            switch (otc)
                            {
                                case 1:
                                    d4.Image = global::Diceroller.Properties.Resources.d4_1;
                                    break;
                                case 2:
                                    d4.Image = global::Diceroller.Properties.Resources.d4_2;
                                    break;
                                case 3:
                                    d4.Image = global::Diceroller.Properties.Resources.d4_3;
                                    break;
                                case 4:
                                    d4.Image = global::Diceroller.Properties.Resources.d4_4;
                                    break;
                            }
                            break;
                        case 6:
                            switch (otc)
                            {
                                case 1:
                                    d4.Image = global::Diceroller.Properties.Resources.d6_1;
                                    break;
                                case 2:
                                    d4.Image = global::Diceroller.Properties.Resources.d6_2;
                                    break;
                                case 3:
                                    d4.Image = global::Diceroller.Properties.Resources.d6_3;
                                    break;
                                case 4:
                                    d4.Image = global::Diceroller.Properties.Resources.d6_4;
                                    break;
                                case 5:
                                    d4.Image = global::Diceroller.Properties.Resources.d6_5;
                                    break;
                                case 6:
                                    d4.Image = global::Diceroller.Properties.Resources.d6_6;
                                    break;
                            }
                            break;
                        case 8:
                            switch (otc)
                            {
                                case 1:
                                    d4.Image = global::Diceroller.Properties.Resources.d8_1;
                                    break;
                                case 2:
                                    d4.Image = global::Diceroller.Properties.Resources.d8_2;
                                    break;
                                case 3:
                                    d4.Image = global::Diceroller.Properties.Resources.d8_3;
                                    break;
                                case 4:
                                    d4.Image = global::Diceroller.Properties.Resources.d8_4;
                                    break;
                                case 5:
                                    d4.Image = global::Diceroller.Properties.Resources.d8_5;
                                    break;
                                case 6:
                                    d4.Image = global::Diceroller.Properties.Resources.d8_6;
                                    break;
                                case 7:
                                    d4.Image = global::Diceroller.Properties.Resources.d8_7;
                                    break;
                                case 8:
                                    d4.Image = global::Diceroller.Properties.Resources.d8_8;
                                    break;
                            }
                            break;
                        case 10:
                            switch (otc)
                            {
                                case 1:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_1;
                                    break;
                                case 2:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_2;
                                    break;
                                case 3:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_3;
                                    break;
                                case 4:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_4;
                                    break;
                                case 5:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_5;
                                    break;
                                case 6:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_6;
                                    break;
                                case 7:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_7;
                                    break;
                                case 8:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_8;
                                    break;
                                case 9:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_9;
                                    break;
                                case 10:
                                    d4.Image = global::Diceroller.Properties.Resources.d10_10;
                                    break;
                            }
                            break;
                        case 12:
                            switch (otc)
                            {
                                case 1:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_1;
                                    break;
                                case 2:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_2;
                                    break;
                                case 3:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_3;
                                    break;
                                case 4:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_4;
                                    break;
                                case 5:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_5;
                                    break;
                                case 6:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_6;
                                    break;
                                case 7:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_7;
                                    break;
                                case 8:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_8;
                                    break;
                                case 9:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_9;
                                    break;
                                case 10:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_10;
                                    break;
                                case 11:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_11;
                                    break;
                                case 12:
                                    d4.Image = global::Diceroller.Properties.Resources.d12_12;
                                    break;
                            }
                            break;
                        case 20:
                            switch (otc)
                            {
                                case 1:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_1;
                                    break;
                                case 2:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_2;
                                    break;
                                case 3:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_3;
                                    break;
                                case 4:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_4;
                                    break;
                                case 5:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_5;
                                    break;
                                case 6:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_6;
                                    break;
                                case 7:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_7;
                                    break;
                                case 8:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_8;
                                    break;
                                case 9:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_9;
                                    break;
                                case 10:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_10;
                                    break;
                                case 11:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_11;
                                    break;
                                case 12:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_12;
                                    break;
                                case 13:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_13;
                                    break;
                                case 14:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_14;
                                    break;
                                case 15:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_15;
                                    break;
                                case 16:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_16;
                                    break;
                                case 17:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_17;
                                    break;
                                case 18:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_18;
                                    break;
                                case 19:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_19;
                                    break;
                                case 20:
                                    d4.Image = global::Diceroller.Properties.Resources.d20_20;
                                    break;
                            }
                            break;
                    }
                    break;
                case 4:
                    switch (end)
                    {
                        case 4:
                            switch (otc)
                            {
                                case 1:
                                    d5.Image = global::Diceroller.Properties.Resources.d4_1;
                                    break;
                                case 2:
                                    d5.Image = global::Diceroller.Properties.Resources.d4_2;
                                    break;
                                case 3:
                                    d5.Image = global::Diceroller.Properties.Resources.d4_3;
                                    break;
                                case 4:
                                    d5.Image = global::Diceroller.Properties.Resources.d4_4;
                                    break;
                            }
                            break;
                        case 6:
                            switch (otc)
                            {
                                case 1:
                                    d5.Image = global::Diceroller.Properties.Resources.d6_1;
                                    break;
                                case 2:
                                    d5.Image = global::Diceroller.Properties.Resources.d6_2;
                                    break;
                                case 3:
                                    d5.Image = global::Diceroller.Properties.Resources.d6_3;
                                    break;
                                case 4:
                                    d5.Image = global::Diceroller.Properties.Resources.d6_4;
                                    break;
                                case 5:
                                    d5.Image = global::Diceroller.Properties.Resources.d6_5;
                                    break;
                                case 6:
                                    d5.Image = global::Diceroller.Properties.Resources.d6_6;
                                    break;
                            }
                            break;
                        case 8:
                            switch (otc)
                            {
                                case 1:
                                    d5.Image = global::Diceroller.Properties.Resources.d8_1;
                                    break;
                                case 2:
                                    d5.Image = global::Diceroller.Properties.Resources.d8_2;
                                    break;
                                case 3:
                                    d5.Image = global::Diceroller.Properties.Resources.d8_3;
                                    break;
                                case 4:
                                    d5.Image = global::Diceroller.Properties.Resources.d8_4;
                                    break;
                                case 5:
                                    d5.Image = global::Diceroller.Properties.Resources.d8_5;
                                    break;
                                case 6:
                                    d5.Image = global::Diceroller.Properties.Resources.d8_6;
                                    break;
                                case 7:
                                    d5.Image = global::Diceroller.Properties.Resources.d8_7;
                                    break;
                                case 8:
                                    d5.Image = global::Diceroller.Properties.Resources.d8_8;
                                    break;
                            }
                            break;
                        case 10:
                            switch (otc)
                            {
                                case 1:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_1;
                                    break;
                                case 2:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_2;
                                    break;
                                case 3:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_3;
                                    break;
                                case 4:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_4;
                                    break;
                                case 5:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_5;
                                    break;
                                case 6:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_6;
                                    break;
                                case 7:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_7;
                                    break;
                                case 8:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_8;
                                    break;
                                case 9:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_9;
                                    break;
                                case 10:
                                    d5.Image = global::Diceroller.Properties.Resources.d10_10;
                                    break;
                            }
                            break;
                        case 12:
                            switch (otc)
                            {
                                case 1:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_1;
                                    break;
                                case 2:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_2;
                                    break;
                                case 3:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_3;
                                    break;
                                case 4:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_4;
                                    break;
                                case 5:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_5;
                                    break;
                                case 6:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_6;
                                    break;
                                case 7:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_7;
                                    break;
                                case 8:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_8;
                                    break;
                                case 9:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_9;
                                    break;
                                case 10:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_10;
                                    break;
                                case 11:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_11;
                                    break;
                                case 12:
                                    d5.Image = global::Diceroller.Properties.Resources.d12_12;
                                    break;
                            }
                            break;
                        case 20:
                            switch (otc)
                            {
                                case 1:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_1;
                                    break;
                                case 2:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_2;
                                    break;
                                case 3:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_3;
                                    break;
                                case 4:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_4;
                                    break;
                                case 5:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_5;
                                    break;
                                case 6:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_6;
                                    break;
                                case 7:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_7;
                                    break;
                                case 8:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_8;
                                    break;
                                case 9:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_9;
                                    break;
                                case 10:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_10;
                                    break;
                                case 11:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_11;
                                    break;
                                case 12:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_12;
                                    break;
                                case 13:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_13;
                                    break;
                                case 14:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_14;
                                    break;
                                case 15:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_15;
                                    break;
                                case 16:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_16;
                                    break;
                                case 17:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_17;
                                    break;
                                case 18:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_18;
                                    break;
                                case 19:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_19;
                                    break;
                                case 20:
                                    d5.Image = global::Diceroller.Properties.Resources.d20_20;
                                    break;
                            }
                            break;
                    }
                    break;
                case 5:
                    switch (end)
                    {
                        case 4:
                            switch (otc)
                            {
                                case 1:
                                    d6.Image = global::Diceroller.Properties.Resources.d4_1;
                                    break;
                                case 2:
                                    d6.Image = global::Diceroller.Properties.Resources.d4_2;
                                    break;
                                case 3:
                                    d6.Image = global::Diceroller.Properties.Resources.d4_3;
                                    break;
                                case 4:
                                    d6.Image = global::Diceroller.Properties.Resources.d4_4;
                                    break;
                            }
                            break;
                        case 6:
                            switch (otc)
                            {
                                case 1:
                                    d6.Image = global::Diceroller.Properties.Resources.d6_1;
                                    break;
                                case 2:
                                    d6.Image = global::Diceroller.Properties.Resources.d6_2;
                                    break;
                                case 3:
                                    d6.Image = global::Diceroller.Properties.Resources.d6_3;
                                    break;
                                case 4:
                                    d6.Image = global::Diceroller.Properties.Resources.d6_4;
                                    break;
                                case 5:
                                    d6.Image = global::Diceroller.Properties.Resources.d6_5;
                                    break;
                                case 6:
                                    d6.Image = global::Diceroller.Properties.Resources.d6_6;
                                    break;
                            }
                            break;
                        case 8:
                            switch (otc)
                            {
                                case 1:
                                    d6.Image = global::Diceroller.Properties.Resources.d8_1;
                                    break;
                                case 2:
                                    d6.Image = global::Diceroller.Properties.Resources.d8_2;
                                    break;
                                case 3:
                                    d6.Image = global::Diceroller.Properties.Resources.d8_3;
                                    break;
                                case 4:
                                    d6.Image = global::Diceroller.Properties.Resources.d8_4;
                                    break;
                                case 5:
                                    d6.Image = global::Diceroller.Properties.Resources.d8_5;
                                    break;
                                case 6:
                                    d6.Image = global::Diceroller.Properties.Resources.d8_6;
                                    break;
                                case 7:
                                    d6.Image = global::Diceroller.Properties.Resources.d8_7;
                                    break;
                                case 8:
                                    d6.Image = global::Diceroller.Properties.Resources.d8_8;
                                    break;
                            }
                            break;
                        case 10:
                            switch (otc)
                            {
                                case 1:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_1;
                                    break;
                                case 2:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_2;
                                    break;
                                case 3:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_3;
                                    break;
                                case 4:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_4;
                                    break;
                                case 5:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_5;
                                    break;
                                case 6:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_6;
                                    break;
                                case 7:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_7;
                                    break;
                                case 8:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_8;
                                    break;
                                case 9:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_9;
                                    break;
                                case 10:
                                    d6.Image = global::Diceroller.Properties.Resources.d10_10;
                                    break;
                            }
                            break;
                        case 12:
                            switch (otc)
                            {
                                case 1:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_1;
                                    break;
                                case 2:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_2;
                                    break;
                                case 3:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_3;
                                    break;
                                case 4:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_4;
                                    break;
                                case 5:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_5;
                                    break;
                                case 6:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_6;
                                    break;
                                case 7:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_7;
                                    break;
                                case 8:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_8;
                                    break;
                                case 9:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_9;
                                    break;
                                case 10:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_10;
                                    break;
                                case 11:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_11;
                                    break;
                                case 12:
                                    d6.Image = global::Diceroller.Properties.Resources.d12_12;
                                    break;
                            }
                            break;
                        case 20:
                            switch (otc)
                            {
                                case 1:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_1;
                                    break;
                                case 2:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_2;
                                    break;
                                case 3:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_3;
                                    break;
                                case 4:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_4;
                                    break;
                                case 5:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_5;
                                    break;
                                case 6:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_6;
                                    break;
                                case 7:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_7;
                                    break;
                                case 8:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_8;
                                    break;
                                case 9:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_9;
                                    break;
                                case 10:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_10;
                                    break;
                                case 11:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_11;
                                    break;
                                case 12:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_12;
                                    break;
                                case 13:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_13;
                                    break;
                                case 14:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_14;
                                    break;
                                case 15:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_15;
                                    break;
                                case 16:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_16;
                                    break;
                                case 17:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_17;
                                    break;
                                case 18:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_18;
                                    break;
                                case 19:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_19;
                                    break;
                                case 20:
                                    d6.Image = global::Diceroller.Properties.Resources.d20_20;
                                    break;
                            }
                            break;
                    }
                    break;
            }
        }
        /// <summary>
        /// Zmienna pozwalająca "przerzucić" wybrany wynik i rysująca kość odpowiadającą nowemu wynikowi.
        /// </summary>
        ///<remarks>
        ///Funkcja korzysta z danych przypisanych do tabeli score.
        ///DO rysowania nowego wyniku na kości wykorzystuje funkcję roll.
        /// </remarks>
        public void reroll(int beg)
        {
            if (score[beg, 2] > 0) { 
                int start = score[beg, 0];
                int end = score[beg, 1];
                score[beg, 2] = rand.Next(start, end + 1);
                int outcome = score[beg, 2];
                rollDraw(beg, end, outcome);
                }
            
        }
        public void reset(int[,] score)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    score[i, j] = 0;
                }
            }
        }
        public void roller(int[,] score)
        {
            for (int i = 0; i < 6; i++)
            {
                score[i, 2] = rand.Next(score[i, 0], score[i, 1] + 1);
                rollDraw(i, score[i, 1], score[i, 2]);
            }
        }

        #endregion

        private void splitContainer2_SplitterMoved(object sender, SplitterEventArgs e)
                    {
                    }
        /// <summary>
        /// Region poświęcony etykietom reprezentującym kości. 
        ///Każda z nich wywołuje funkcję reroll przy kliknięciu.
        /// </summary>
        #region Dices
        private void d1_Click(object sender, EventArgs e)
                    {
            reroll(0);

        }

                    private void d2_Click(object sender, EventArgs e)
                    {
            reroll(1);
                    }

                    private void d3_Click(object sender, EventArgs e)
                    {
            reroll(2);
        }

                    private void d4_Click(object sender, EventArgs e)
                    {
            reroll(3);
        }

                    private void d5_Click(object sender, EventArgs e)
                    {
            reroll(4);
        }

                    private void d6_Click(object sender, EventArgs e)
                    {
            reroll(5);
        }
        #endregion
        /// <summary>
        /// Region poświęcony przyciskom pozwalającym wybrać kości.
        /// </summary>
        ///<remarks>
        ///Przy kliknięciu w przycisk, ten przypisuje zakres kości do miejsca w tabeli score  określanego przez zmienną caunt.
        ///Wywołuje także funkcję blank z pierwszym parametrem pobranym ze zmiennej caunt a drugim określonym przez zakres kości.
        /// </remarks>

        #region DiceSellector

        private void D4B_Click(object sender, EventArgs e)
                    {
                        score[caunt, 0] = 1;
                        score[caunt, 1] = 4;
                        blank(caunt, 4);

            // Instrukca if, która sprawdza wysokość zmiennej caunt <see cref="CTD.caunt"/> i zwiększa ją o 1 jeśli ta jest mniejsza niż 5.

            if (caunt < 5)
            {
                caunt++;
            }

        }

                    private void D6B_Click(object sender, EventArgs e)
                    {
                        score[caunt, 0] = 1;
                        score[caunt, 1] = 6;
            blank(caunt, 6);
            if (caunt < 5)
            {
                caunt++;
            }
        }

                    private void D8B_Click(object sender, EventArgs e)
                    {
                        score[caunt, 0] = 1;
                        score[caunt, 1] = 8;
            blank(caunt, 8);
            if (caunt < 5)
            {
                caunt++;
            }
        }

                    private void D10B_Click(object sender, EventArgs e)
                    {
                        score[caunt, 0] = 1;
                        score[caunt, 1] = 10;
            blank(caunt, 10);
            if (caunt < 5)
            {
                caunt++;
            }
        }

                    private void D12B_Click(object sender, EventArgs e)
                    {
                        score[caunt, 0] = 1;
                        score[caunt, 1] = 12;
            blank(caunt, 12);
            if (caunt < 5)
            {
                caunt++;
            }
        }

                    private void D20B_Click(object sender, EventArgs e)
                    {
                        score[caunt, 0] = 1;
                        score[caunt, 1] = 20;
            blank(caunt, 20);
            if (caunt < 5)
            {
                caunt++;
            }
                    }
        #endregion

        #region ResAndRoll
        ///<summary>
        /// Metoda, która działa po wciśnięciu przycisku losowania kości.
        ///</summary>
        /// <remarks>
        /// Pętla for "przechodzi" kolejno po tablicy score i pobiera z niej elementy przypisane do niej przez wcześniej wciśnięte przyciski wyboru kości.
        /// Do trzeciego pola każdego elementu tablicy przypisuje ona wynik działania rand.Next, którego zmiennymi są liczby znajdujące sie na pierwszym i drugim polu tego elementu.
        /// Liczba z drugiego pola ma dodaną cyfrę 1, gdyż rand.Next(a,b) może maksymalnie uzyskać wynik o 1 mniejszy od zmiennej b.
        /// Następnie metoda wywołuje funkcję roll, w której argumentami są i - poziom iteracjii pętli, end - maksymalny zakres kości, oraz score[i, 2] czyli liczba z trzeciego pola każdego elementu tablicy score.
        /// </remarks>

        private void Roll_Click(object sender, EventArgs e)
    {
        roller(score);
        }
        ///<summary>
        /// Zadaniem tej metody jest resetowanie programu poprzez wprowadzenie danych pierwotnych.
        /// </summary>
        /// <remarks>
        /// Zagnieżdzona pętla for ustawia wartości 0 dla każego atrybutu tablicy score 
        /// Następnie polecenie d1.Image = null i kolejne, zerują grafiki dla wszystkich etykiet.
        /// Jako ostatnią funkcja zeruje zmienną caunt
        /// </remarks>

                    private void Reset_Click(object sender, EventArgs e)
                    {
            reset(score);
            d1.Image = null;
            d2.Image = null;
            d3.Image = null;
            d4.Image = null;
            d5.Image = null;
            d6.Image = null;
            caunt = 0;
                    }
            }
        }
#endregion

