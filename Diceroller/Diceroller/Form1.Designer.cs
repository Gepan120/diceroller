﻿using System;

namespace Diceroller
{
    partial class CTD
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.d6 = new System.Windows.Forms.Label();
            this.d5 = new System.Windows.Forms.Label();
            this.d4 = new System.Windows.Forms.Label();
            this.d3 = new System.Windows.Forms.Label();
            this.d2 = new System.Windows.Forms.Label();
            this.d1 = new System.Windows.Forms.Label();
            this.Message = new System.Windows.Forms.Label();
            this.Reset = new System.Windows.Forms.Button();
            this.Roll = new System.Windows.Forms.Button();
            this.D20B = new System.Windows.Forms.Button();
            this.D12B = new System.Windows.Forms.Button();
            this.D10B = new System.Windows.Forms.Button();
            this.D8B = new System.Windows.Forms.Button();
            this.D6B = new System.Windows.Forms.Button();
            this.D4B = new System.Windows.Forms.Button();
            this.Choose = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.d6);
            this.splitContainer1.Panel1.Controls.Add(this.d5);
            this.splitContainer1.Panel1.Controls.Add(this.d4);
            this.splitContainer1.Panel1.Controls.Add(this.d3);
            this.splitContainer1.Panel1.Controls.Add(this.d2);
            this.splitContainer1.Panel1.Controls.Add(this.d1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Message);
            this.splitContainer1.Panel2.Controls.Add(this.Reset);
            this.splitContainer1.Panel2.Controls.Add(this.Roll);
            this.splitContainer1.Panel2.Controls.Add(this.D20B);
            this.splitContainer1.Panel2.Controls.Add(this.D12B);
            this.splitContainer1.Panel2.Controls.Add(this.D10B);
            this.splitContainer1.Panel2.Controls.Add(this.D8B);
            this.splitContainer1.Panel2.Controls.Add(this.D6B);
            this.splitContainer1.Panel2.Controls.Add(this.D4B);
            this.splitContainer1.Panel2.Controls.Add(this.Choose);
            this.splitContainer1.Size = new System.Drawing.Size(884, 561);
            this.splitContainer1.SplitterDistance = 163;
            this.splitContainer1.TabIndex = 0;
            // 
            // d6
            // 
            this.d6.AutoSize = true;
            this.d6.Location = new System.Drawing.Point(715, 9);
            this.d6.MaximumSize = new System.Drawing.Size(128, 128);
            this.d6.MinimumSize = new System.Drawing.Size(128, 128);
            this.d6.Name = "d6";
            this.d6.Size = new System.Drawing.Size(128, 128);
            this.d6.TabIndex = 5;
            this.d6.Click += new System.EventHandler(this.d6_Click);
            // 
            // d5
            // 
            this.d5.AutoSize = true;
            this.d5.Location = new System.Drawing.Point(581, 9);
            this.d5.MaximumSize = new System.Drawing.Size(128, 128);
            this.d5.MinimumSize = new System.Drawing.Size(128, 128);
            this.d5.Name = "d5";
            this.d5.Size = new System.Drawing.Size(128, 128);
            this.d5.TabIndex = 4;
            this.d5.Click += new System.EventHandler(this.d5_Click);
            // 
            // d4
            // 
            this.d4.AutoSize = true;
            this.d4.Location = new System.Drawing.Point(447, 9);
            this.d4.MaximumSize = new System.Drawing.Size(128, 128);
            this.d4.MinimumSize = new System.Drawing.Size(128, 128);
            this.d4.Name = "d4";
            this.d4.Size = new System.Drawing.Size(128, 128);
            this.d4.TabIndex = 3;
            this.d4.Click += new System.EventHandler(this.d4_Click);
            // 
            // d3
            // 
            this.d3.AutoSize = true;
            this.d3.Location = new System.Drawing.Point(313, 9);
            this.d3.MaximumSize = new System.Drawing.Size(128, 128);
            this.d3.MinimumSize = new System.Drawing.Size(128, 128);
            this.d3.Name = "d3";
            this.d3.Size = new System.Drawing.Size(128, 128);
            this.d3.TabIndex = 2;
            this.d3.Click += new System.EventHandler(this.d3_Click);
            // 
            // d2
            // 
            this.d2.AutoSize = true;
            this.d2.Location = new System.Drawing.Point(179, 9);
            this.d2.MaximumSize = new System.Drawing.Size(128, 128);
            this.d2.MinimumSize = new System.Drawing.Size(128, 128);
            this.d2.Name = "d2";
            this.d2.Size = new System.Drawing.Size(128, 128);
            this.d2.TabIndex = 1;
            this.d2.Click += new System.EventHandler(this.d2_Click);
            // 
            // d1
            // 
            this.d1.AutoSize = true;
            this.d1.Location = new System.Drawing.Point(45, 9);
            this.d1.MaximumSize = new System.Drawing.Size(128, 128);
            this.d1.MinimumSize = new System.Drawing.Size(128, 128);
            this.d1.Name = "d1";
            this.d1.Size = new System.Drawing.Size(128, 128);
            this.d1.TabIndex = 0;
            this.d1.Click += new System.EventHandler(this.d1_Click);
            // 
            // Message
            // 
            this.Message.AutoSize = true;
            this.Message.Font = new System.Drawing.Font("Cooper Black", 10F);
            this.Message.Location = new System.Drawing.Point(541, 369);
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(331, 16);
            this.Message.TabIndex = 9;
            this.Message.Text = "You can re-roll your dices by clicking on them";
            // 
            // Reset
            // 
            this.Reset.Font = new System.Drawing.Font("Cooper Black", 14.25F);
            this.Reset.Location = new System.Drawing.Point(584, 163);
            this.Reset.MaximumSize = new System.Drawing.Size(200, 50);
            this.Reset.MinimumSize = new System.Drawing.Size(200, 50);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(200, 50);
            this.Reset.TabIndex = 8;
            this.Reset.Text = "New roll";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // Roll
            // 
            this.Roll.Font = new System.Drawing.Font("Cooper Black", 14.25F);
            this.Roll.Location = new System.Drawing.Point(584, 76);
            this.Roll.MaximumSize = new System.Drawing.Size(200, 50);
            this.Roll.MinimumSize = new System.Drawing.Size(200, 50);
            this.Roll.Name = "Roll";
            this.Roll.Size = new System.Drawing.Size(200, 50);
            this.Roll.TabIndex = 7;
            this.Roll.Text = "Roll dices!";
            this.Roll.UseVisualStyleBackColor = true;
            this.Roll.Click += new System.EventHandler(this.Roll_Click);
            // 
            // D20B
            // 
            this.D20B.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.D20B.Font = new System.Drawing.Font("Cooper Black", 14.25F);
            this.D20B.Location = new System.Drawing.Point(161, 210);
            this.D20B.MaximumSize = new System.Drawing.Size(100, 50);
            this.D20B.MinimumSize = new System.Drawing.Size(100, 50);
            this.D20B.Name = "D20B";
            this.D20B.Size = new System.Drawing.Size(100, 50);
            this.D20B.TabIndex = 6;
            this.D20B.Text = "D20";
            this.D20B.UseVisualStyleBackColor = true;
            this.D20B.Click += new System.EventHandler(this.D20B_Click);
            // 
            // D12B
            // 
            this.D12B.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.D12B.Font = new System.Drawing.Font("Cooper Black", 14.25F);
            this.D12B.Location = new System.Drawing.Point(48, 210);
            this.D12B.MaximumSize = new System.Drawing.Size(100, 50);
            this.D12B.MinimumSize = new System.Drawing.Size(100, 50);
            this.D12B.Name = "D12B";
            this.D12B.Size = new System.Drawing.Size(100, 50);
            this.D12B.TabIndex = 5;
            this.D12B.Text = "D12";
            this.D12B.UseVisualStyleBackColor = true;
            this.D12B.Click += new System.EventHandler(this.D12B_Click);
            // 
            // D10B
            // 
            this.D10B.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.D10B.Font = new System.Drawing.Font("Cooper Black", 14.25F);
            this.D10B.Location = new System.Drawing.Point(161, 144);
            this.D10B.MaximumSize = new System.Drawing.Size(100, 50);
            this.D10B.MinimumSize = new System.Drawing.Size(100, 50);
            this.D10B.Name = "D10B";
            this.D10B.Size = new System.Drawing.Size(100, 50);
            this.D10B.TabIndex = 4;
            this.D10B.Text = "D10";
            this.D10B.UseVisualStyleBackColor = true;
            this.D10B.Click += new System.EventHandler(this.D10B_Click);
            // 
            // D8B
            // 
            this.D8B.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.D8B.Font = new System.Drawing.Font("Cooper Black", 14.25F);
            this.D8B.Location = new System.Drawing.Point(46, 144);
            this.D8B.MaximumSize = new System.Drawing.Size(100, 50);
            this.D8B.MinimumSize = new System.Drawing.Size(100, 50);
            this.D8B.Name = "D8B";
            this.D8B.Size = new System.Drawing.Size(100, 50);
            this.D8B.TabIndex = 3;
            this.D8B.Text = "D8";
            this.D8B.UseVisualStyleBackColor = true;
            this.D8B.Click += new System.EventHandler(this.D8B_Click);
            // 
            // D6B
            // 
            this.D6B.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.D6B.Font = new System.Drawing.Font("Cooper Black", 14.25F);
            this.D6B.Location = new System.Drawing.Point(161, 76);
            this.D6B.MaximumSize = new System.Drawing.Size(100, 50);
            this.D6B.MinimumSize = new System.Drawing.Size(100, 50);
            this.D6B.Name = "D6B";
            this.D6B.Size = new System.Drawing.Size(100, 50);
            this.D6B.TabIndex = 2;
            this.D6B.Text = "D6";
            this.D6B.UseVisualStyleBackColor = true;
            this.D6B.Click += new System.EventHandler(this.D6B_Click);
            // 
            // D4B
            // 
            this.D4B.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.D4B.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.D4B.Location = new System.Drawing.Point(46, 76);
            this.D4B.MaximumSize = new System.Drawing.Size(100, 50);
            this.D4B.MinimumSize = new System.Drawing.Size(100, 50);
            this.D4B.Name = "D4B";
            this.D4B.Size = new System.Drawing.Size(100, 50);
            this.D4B.TabIndex = 1;
            this.D4B.Text = "D4";
            this.D4B.UseVisualStyleBackColor = true;
            this.D4B.Click += new System.EventHandler(this.D4B_Click);
            // 
            // Choose
            // 
            this.Choose.AutoSize = true;
            this.Choose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Choose.Font = new System.Drawing.Font("Cooper Black", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Choose.Location = new System.Drawing.Point(42, 20);
            this.Choose.Name = "Choose";
            this.Choose.Size = new System.Drawing.Size(194, 31);
            this.Choose.TabIndex = 0;
            this.Choose.Text = "Choose dices:";
            // 
            // CTD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.splitContainer1);
            this.MaximumSize = new System.Drawing.Size(900, 600);
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "CTD";
            this.Text = "DiceRoller";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label d6;
        private System.Windows.Forms.Label d5;
        private System.Windows.Forms.Label d4;
        private System.Windows.Forms.Label d3;
        private System.Windows.Forms.Label d2;
        private System.Windows.Forms.Label d1;
        private System.Windows.Forms.Button D8B;
        private System.Windows.Forms.Button D6B;
        private System.Windows.Forms.Button D4B;
        private System.Windows.Forms.Label Choose;
        private System.Windows.Forms.Button D20B;
        private System.Windows.Forms.Button D12B;
        private System.Windows.Forms.Button D10B;
        private System.Windows.Forms.Label Message;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button Roll;
    }
}

