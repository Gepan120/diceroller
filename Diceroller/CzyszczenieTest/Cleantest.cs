﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Diceroller;
using System.Globalization;
using System.Security.Cryptography;

namespace CzyszczenieTest
{
    [TestClass]
    public class Cleantest
    {
        [TestMethod]
        public void clean()
        {
            CTD test = new CTD();
            int[,] score = new int[6, 3] { { 1, 6, 3 }, { 1, 12, 10 }, { 1, 6, 6 }, { 1, 20, 1 }, { 1, 8, 4 }, { 1, 4, 3 } };
            int[,] expect = new int[6, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
            test.reset(score);
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Assert.AreEqual(score[i, j], expect[i, j]);


                }
            }
        }
    }
}




