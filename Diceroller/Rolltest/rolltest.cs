﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Diceroller;

namespace Rolltest
{
    [TestClass]
    public class rolltest
    {
        [TestMethod]
        public void Rollertes()
        {
            CTD test = new CTD();
            int[,] score = new int[6, 3] { { 1, 6, 0 }, { 1, 12, 0 }, { 1, 6, 0 }, { 1, 20, 0 }, { 1, 8, 0 }, { 1, 4, 0 } };
            int[,] notexpect = new int[6, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
            test.roller(score);
            for (int i = 0; i < 6; i++)
            {
                Assert.AreNotEqual(score[i, 2], notexpect[i, 2]);


                
            }
        }
    }
}
